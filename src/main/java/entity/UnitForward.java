package entity;

import java.sql.Timestamp;

public class UnitForward{
    private Long id;

    private String unit_code;

    private String url;

    private String url_auth;

    private String document_type_code;

    private String method;

    private Timestamp created_at;

    private Timestamp updated_at;

    private String secret_key;

    private String public_key;

    private String other_info;

    private String pre_process_class;


    public String getUnit_code() {
        return unit_code;
    }

    public void setUnit_code(String unit_code) {
        this.unit_code = unit_code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl_auth() {
        return url_auth;
    }

    public void setUrl_auth(String url_auth) {
        this.url_auth = url_auth;
    }

    public String getDocument_type_code() {
        return document_type_code;
    }

    public void setDocument_type_code(String document_type_code) {
        this.document_type_code = document_type_code;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getPublic_key() {
        return public_key;
    }

    public void setPublic_key(String public_key) {
        this.public_key = public_key;
    }

    public String getOther_info() {
        return other_info;
    }

    public void setOther_info(String other_info) {
        this.other_info = other_info;
    }

    public String getPre_process_class() {
        return pre_process_class;
    }

    public void setPre_process_class(String pre_process_class) {
        this.pre_process_class = pre_process_class;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UnitForward(Long id, String unit_code, String url, String url_auth, String document_type_code, String method, Timestamp created_at, Timestamp updated_at, String secret_key, String public_key, String other_info, String pre_process_class) {
        this.id = id;
        this.unit_code = unit_code;
        this.url = url;
        this.url_auth = url_auth;
        this.document_type_code = document_type_code;
        this.method = method;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.secret_key = secret_key;
        this.public_key = public_key;
        this.other_info = other_info;
        this.pre_process_class = pre_process_class;
    }

    public UnitForward() {
    }
}
