package entity;

public class RequiredModel {
	private String title;
	private String document_type;
	private String from_unit_code;
	private String to_unit_code;
	
	public RequiredModel() {
		super();
	}
	public RequiredModel(String title, String document_type, String from_unit_code, String to_unit_code) {
		super();
		this.title = title;
		this.document_type = document_type;
		this.from_unit_code = from_unit_code;
		this.to_unit_code = to_unit_code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDocument_type() {
		return document_type;
	}
	public void setDocument_type(String document_type) {
		this.document_type = document_type;
	}
	public String getFrom_unit_code() {
		return from_unit_code;
	}
	public void setFrom_unit_code(String from_unit_code) {
		this.from_unit_code = from_unit_code;
	}
	public String getTo_unit_code() {
		return to_unit_code;
	}
	public void setTo_unit_code(String to_unit_code) {
		this.to_unit_code = to_unit_code;
	}
}
