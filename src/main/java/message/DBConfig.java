package message;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DBConfig {
    public static String file_pathDir = "1";
    public static String mysql_driver = "1";
    public static String mysql_username = "1";
    public static String mysql_password = "1";
    public static String mysql_url = "1";
    public static String server_port = "1";
    public static String spring_activemq_broker_url = "1";
    public static String spring_activemq_user = "1";
    public static String spring_activemq_password = "1";
    public static String spring_jms_pub_sub_domain = "1";
    public static String spring_activemq_queue = "1";



    private static Properties properties = new Properties();

    public static void loadProperties() throws IOException {
        FileInputStream propFile = new java.io.FileInputStream("./conf/application.properties");
        properties.load(propFile);
        propFile.close();

        file_pathDir = properties.getProperty("file.upload.dir", file_pathDir);
        mysql_driver = properties.getProperty("db.mysql.driver", mysql_driver);
        mysql_username = properties.getProperty("db.mysql.user", mysql_username);
        mysql_password = properties.getProperty("db.mysql.pass", mysql_password);
        mysql_url = properties.getProperty("db.mysql.url", mysql_url);
        server_port = properties.getProperty("server.port", server_port);
        spring_activemq_broker_url = properties.getProperty("spring.activemq.broker-url", spring_activemq_broker_url);
        spring_activemq_user = properties.getProperty("spring.activemq.user", spring_activemq_user);
        spring_activemq_password = properties.getProperty("spring.activemq.password", spring_activemq_password);
        spring_jms_pub_sub_domain = properties.getProperty("spring.jms.pub-sub-domain", spring_jms_pub_sub_domain);
        spring_activemq_queue = properties.getProperty("spring.activemq.queue", spring_activemq_queue);

    }

    static {
        try {
            DBConfig.loadProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
