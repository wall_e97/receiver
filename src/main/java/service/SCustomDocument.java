package service;

import java.util.HashMap;
import java.util.Optional;

import entity.UnitForward;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import repository.ConnectDB;
import ultil.HttpClient;

@Service
public class SCustomDocument{

	public void customDocument(String toUnitCode, String documentType, String content, String fileName,
							   HashMap<String, String> textRequired) throws Exception {

		Optional<UnitForward> op = ConnectDB.getUnitForward(toUnitCode, documentType);
		String url = op.get().getUrl();
		HttpClient.httpPostFile(url, content, fileName, textRequired);
	}
}
