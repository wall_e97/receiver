package message;
import com.google.gson.Gson;

import entity.UnitForward;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import repository.ConnectDB;
import service.SCustomDocument;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class Listener {
    @JmsListener(destination = "foo.bar")
    public void receiveMessage(Message jsonMessage) throws Exception {
        String pathDir = DBConfig.file_pathDir;
        SCustomDocument sCustomDocument = new SCustomDocument();
        String messageData = null;
        if(jsonMessage instanceof TextMessage) {
            System.out.println("Nhận tin nhắn: " + jsonMessage);
            TextMessage textMessage = (TextMessage)jsonMessage;
            messageData = textMessage.getText();


            Map map = new Gson().fromJson(messageData, Map.class);

            String title = map.get("title").toString();
            String from_unit_code = map.get("from_unit_code").toString();
            String document_type = map.get("document_type").toString();
            String to_unit_code = map.get("to_unit_code").toString();

            Long time = System.currentTimeMillis();
            String fileName = title + time.toString();
            try {
                FileOutputStream out = new FileOutputStream(pathDir + fileName + ".txt");
                out.write(messageData.getBytes());
                out.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }

            System.out.println(fileName);
            HashMap<String, String> paramRequired = new HashMap<String, String>();
            paramRequired.put("title", title);
            paramRequired.put("document_type", document_type);
            paramRequired.put("from_unit_code", from_unit_code);
            paramRequired.put("to_unit_code", to_unit_code);
            sCustomDocument.customDocument(to_unit_code, document_type, messageData, fileName, paramRequired);
        }
    }
}
