package repository;

import entity.UnitForward;
import message.DBConfig;
import org.springframework.beans.factory.annotation.Value;

import java.sql.*;
import java.util.Optional;

public class ConnectDB {
    private static Connection connect = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    private static String urlConnect = DBConfig.mysql_url;
    private static String driver = DBConfig.mysql_driver;
    private static String username = DBConfig.mysql_username;
    private static String password = DBConfig.mysql_password;

    private Connection getConnection() {
        try {
            System.out.println("----------------------->" + urlConnect);
            Class.forName(driver);
            // Setup the connection with the DB
            connect = DriverManager
                    .getConnection(urlConnect + "?" + "user=" + username + "&" + "password=" +  password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        System.out.println(DBConfig.server_port);
        return connect;
    }

    public static Optional<UnitForward> getUnitForward(String toUnitCode, String documentType) throws Exception{
        ResultSet resultSet = null;
        try {
            ConnectDB connectDB = new ConnectDB();
            String sqlQuery = "SELECT * FROM unit_forward_url u WHERE u.unit_code = ? and u.document_type_code = ? ;";
            PreparedStatement pstmt = connectDB.getConnection().prepareStatement(sqlQuery);
            pstmt.setString(1, toUnitCode);
            pstmt.setString(2, documentType);
            resultSet = pstmt.executeQuery();
            if (resultSet.next()){
                return Optional.of(createUnitForward(resultSet));
            } else {
                return Optional.empty();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return Optional.empty();
    }

    public static void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

    private static UnitForward createUnitForward(ResultSet resultSet) throws SQLException {
        return new UnitForward(
                resultSet.getLong("id"),
                resultSet.getString("unit_code"),
                resultSet.getString("url"),
                resultSet.getString("url_auth"),
                resultSet.getString("document_type_code"),
                resultSet.getString("method"),
                resultSet.getTimestamp("created_at"),
                resultSet.getTimestamp("updated_at"),
                resultSet.getString("secret_key"),
                resultSet.getString("public_key"),
                resultSet.getString("other_info"),
                resultSet.getString("pre_process_class"));
    }
}
